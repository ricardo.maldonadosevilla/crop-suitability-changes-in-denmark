# Crop suitability changes in Denmark

This repository is meant to show the R scripts that were written by Group 4 for the project of Spatial and Temporal Modelling at Wageningen University & Research. The input data for the scripts is not provided in this repository.